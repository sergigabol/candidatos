var imgIndex = 0;
var overlayOne = null;
var imgOverlayOne = null;
var pageOne = null;

var WorldSanchez1 = {
	loaded: false,

	init: function initFn() {
		this.createOverlays();
	},
	createOverlays: function createOverlaysFn() {

	    /* Utilizamos una función externa para crear una imagen, que al hacer click sobre ella
	        se sustituya.
	    */

        setImage("sanchez/assets/bigote-tupido.png",0.036,0.046,0.15);

		//Declaramos las imágenes para la animación del santander.
   		var imgSantander = new AR.ImageResource("sanchez/assets/logo-santander.png");
   		var imgBlancoOjosIzq = new AR.ImageResource("sanchez/assets/negroOjoIzqSanchez.png");
   		var imgBlancoOjosDer = new AR.ImageResource("sanchez/assets/negroOjoDerSanchez.png");
   		var ojosIzq = new AR.ImageResource("sanchez/assets/ojoIzqSanchez.png");
   		var ojosDer = new AR.ImageResource("sanchez/assets/ojoDerSanchez.png");

   		var despXOjos = 0.055;

		//Las metemos en objetos dibujables (má o meno):
		var overlaySantander = new AR.ImageDrawable(imgSantander, 0.10,
			{
				offsetX: 0.3,
				offsetY: 0.3
			});
		var overlayBlancoOjosIzq = new AR.ImageDrawable(imgBlancoOjosIzq, 0.03,
        			{
        				offsetX: -0.03,
        				offsetY: 0.145
        			});
		var overlayBlancoOjosDer = new AR.ImageDrawable(imgBlancoOjosDer, 0.03,
        			{
        				offsetX: 0.085,
        				offsetY: 0.145
        			});
		var overlayOjosIzq = new AR.ImageDrawable(ojosIzq, 0.03,
        			{
        				offsetX: -0.03,
        				offsetY: 0.145
        			});
		var overlayOjosDer = new AR.ImageDrawable(ojosDer, 0.03,
        			{
        				offsetX: 0.085,
        				offsetY: 0.145
        			});

		//Creamos las animaciones individuales para cada imagen animada.
			//Animación inicial de los ojos.
		var ojosIzqIniXAnim = new AR.PropertyAnimation(
			overlayOjosIzq, "offsetX", -0.03, (-0.03+despXOjos), 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosIzqIniYAnim = new AR.PropertyAnimation(
          	overlayOjosIzq, "offsetY", 0.145, 0.165, 1500,
          	{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
		var ojosDerIniXAnim = new AR.PropertyAnimation(
			overlayOjosDer, "offsetX", 0.085, (0.085+despXOjos), 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosDerIniYAnim = new AR.PropertyAnimation(
          	overlayOjosDer, "offsetY", 0.145, 0.165, 1500,
          	{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );

        	//Animación subiendo (logo y ojos).
		var santanderXAnimUp = new AR.PropertyAnimation(
			overlaySantander, "offsetX", 0.3, 0.0, 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var santanderYAnimUp = new AR.PropertyAnimation(
        	overlaySantander, "offsetY", 0.3, 0.5, 1500,
        	{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
		var ojosIzqXAnimUp = new AR.PropertyAnimation(
			overlayOjosIzq, "offsetX", (-0.03+despXOjos), -0.03, 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosIzqYAnimUp = new AR.PropertyAnimation(
             overlayOjosIzq, "offsetY", 0.165, 0.180, 1500,
             {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
		var ojosDerXAnimUp = new AR.PropertyAnimation(
			overlayOjosDer, "offsetX", (0.085+despXOjos), 0.085, 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosDerYAnimUp = new AR.PropertyAnimation(
             overlayOjosDer, "offsetY", 0.165, 0.180, 1500,
             {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );

        	//Animación bajando (logo y ojos).
		var santanderXAnimDown = new AR.PropertyAnimation(
			overlaySantander, "offsetX", 0.0, -0.3, 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var santanderYAnimDown = new AR.PropertyAnimation(
            overlaySantander, "offsetY", 0.5, 0.3, 1500,
            {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
		var ojosIzqXAnimDown = new AR.PropertyAnimation(
			overlayOjosIzq, "offsetX", -0.03, (-0.03-despXOjos), 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosIzqYAnimDown = new AR.PropertyAnimation(
            overlayOjosIzq, "offsetY", 0.180, 0.165, 1500,
            {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
		var ojosDerXAnimDown = new AR.PropertyAnimation(
			overlayOjosDer, "offsetX", 0.085, (0.085-despXOjos), 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosDerYAnimDown = new AR.PropertyAnimation(
            overlayOjosDer, "offsetY", 0.180, 0.165, 1500,
            {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );

        //Unimos las animaciones individuales en grupos coherentes (comparten el momento de animación) y
        //lanzando a través del evento onFinish el grupo de animación siguiente.
        var ojosIniAnim =  new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, // the animations will run in parallel
            [ojosIzqIniXAnim, ojosIzqIniYAnim, ojosDerIniXAnim, ojosDerIniYAnim],{ //Las animaciones individuales.
            //Al iniciar la animación colocamos la imagen del santander en su posición inicial.
        	onStart : function(){overlaySantander.offsetX = 0.3; overlaySantander.offSetY = 0.3;},
        	onFinish : function(){santanderAnimationUp.start()}
            }
        );
		var santanderAnimationUp = new AR.AnimationGroup(
            AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, // the animations will run in parallel
            [santanderXAnimUp, santanderYAnimUp, ojosIzqXAnimUp, ojosIzqYAnimUp, ojosDerXAnimUp, ojosDerYAnimUp],
            {onFinish : function(){santanderAnimationDown.start()}} //
        );
        var santanderAnimationDown = new AR.AnimationGroup(
            AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, // the animations will run in parallel
            [santanderXAnimDown, santanderYAnimDown, ojosIzqXAnimDown, ojosIzqYAnimDown, ojosDerXAnimDown, ojosDerYAnimDown], // the animations in the AnimationGroup
            {onFinish : function(){ojosIniAnim.start()}} //when finished, play a beep sound that elevators have reached their positions
        );




		/*
			The last line combines everything by creating an AR.Trackable2DObject with the previously created tracker, the name of the image target and the drawable that should augment the recognized image.
			Please note that in this case the target name is a wildcard. Wildcards can be used to respond to any target defined in the target collection. If you want to respond to a certain target only for a particular AR.Trackable2DObject simply provide the target name as specified in the target collection.
		*/
		var pageTwo = new AR.Trackable2DObject(tracker, "sanchez",
		{
                renderingOrder :2,
				drawables:{cam: [overlaySantander, overlayBlancoOjosIzq, overlayBlancoOjosDer, overlayOjosIzq, overlayOjosDer]}
		});
		//La animación comienza con esta.
		ojosIniAnim.start();
	}
};

function setImage(imatge, posX, posY, escala){

    // Si no és el primer cop que entra, esborra els objectes ja creats anteriorment
    if (imgIndex != 0){
        pageOne.drawables.removeCamDrawable(overlayOne);
        overlayOne.destroy();
        imgOverlayOne.destroy();
    }else{
        imgIndex = 2; //index 2 porque por defecto se carga el 1
    }

    /* Create overlay for page one */
	imgOverlayOne = new AR.ImageResource(imatge);
    overlayOne = new AR.ImageDrawable(imgOverlayOne, escala,
		    {
			    offsetX: posX,
			    offsetY: posY,
			    onClick: function (){

			        if (imgIndex == 1) setImage("sanchez/assets/bigote-tupido.png",0.036,0.046,0.15);
			        if (imgIndex == 2) setImage("sanchez/assets/bigote-medio.png",0.036,0.046,0.15);
			        if (imgIndex == 3) setImage("sanchez/assets/bigot-fino.png",0.036,0.046,0.15);
			        if (imgIndex == 4) setImage("sanchez/assets/mascara_pallaso.png",0.032,0.14,0.55); // pallaso
			        if (imgIndex == 5) setImage("sanchez/assets/mascara_pirata_1.png",0.029,0.32,0.3); // Barret
			        if (imgIndex == 6) setImage("sanchez/assets/mascara_pirata_2.png",-0.04,-0.4,0.8); // Vestit
			        if (imgIndex == 7) setImage("sanchez/assets/mascara_pirata_3.png",0.013,0.17,0.16); // Parxe ull esquerra
			        if (imgIndex == 8) setImage("sanchez/assets/mascara_pirata_4.png",0.02,0.35,0.3); //Barret enorme
			        imgIndex = (imgIndex == 8) ? 1 : imgIndex+1;
			    }
            }
	);

    if (pageOne != null){
        pageOne.drawables.addCamDrawable(overlayOne);
    }else{
	    pageOne = new AR.Trackable2DObject(tracker, "sanchez",
                {
                    drawables: {cam:overlayOne}
                }

        );
    }
}

WorldSanchez1.init();
