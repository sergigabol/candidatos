var imgIglesias = 1;
var tiempoTouchInicial = 0;

var WorldIglesias={

    init:function(){

        // IMAGENES de BROMA
        var cowboyHat = new AR.ImageResource("iglesias/CowboyHat.png");
        var hipsterGlasses = new AR.ImageResource("iglesias/GafasHipster.png");
        var imgSprite = new AR.ImageResource("iglesias/SpriteExplosion2.jpg");
        var bolaDeCristal = new AR.ImageResource("iglesias/boladecristal.png");
        var gorroBruja = new AR.ImageResource("iglesias/gorroBruja.png");

        // OVERLAYS
        this.overlayCowboy = new AR.ImageDrawable(cowboyHat,0.5,{offsetX:-0.05,offsetY:0.55, rotation:-18});
        this.overlayGlasses = new AR.ImageDrawable(hipsterGlasses,0.3,{offsetX:0,offsetY:0.32, rotation:-10});
        this.overlayBolaIzq = new AR.ImageDrawable(bolaDeCristal, 0.3, {offsetX:-0.7, offsetY:-0.3});
        this.overlayBolaDer = new AR.ImageDrawable(bolaDeCristal, 0.3, {offsetX:0.75, offsetY:-0.25});
        this.overlayGorro = new AR.ImageDrawable(gorroBruja, 0.6, {offsetX:0.0, offsetY:0.65});

        this.spriteExplosion = new AR.AnimatedImageDrawable(
                    imgSprite,
                    1,
                    96,
                    96,
                        {
                            offsetX: 0,
                            offsetY: 0,
                            rotation: 0
                        }
                    );

        // Animar la explosion
        this.spriteExplosion.animate([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], 100, -1);

        // INICIAR EL TRACKER
        this.pageOne = new AR.Trackable2DObject(
                    tracker,
                    "pabloiglesias",
                    {
                        drawables: { cam: WorldIglesias.overlayCowboy },
                        onClick: WorldIglesias.toggleAnimateModel
                    }
                );
    },

    toggleAnimateModel: function toggleAnimateModel() {

        var f = new Date();
        var tiempoTouch = (f.getHours() * 3600) + (f.getMinutes() *60)+ f.getSeconds();

        if(tiempoTouch - tiempoTouchInicial > 1){
            if(imgIglesias == 1){
                WorldIglesias.pageOne.drawables.removeCamDrawable(WorldIglesias.overlayCowboy);
                WorldIglesias.pageOne.drawables.addCamDrawable(WorldIglesias.overlayGlasses);
                imgIglesias++;
            } else if (imgIglesias == 2){
                WorldIglesias.pageOne.drawables.removeCamDrawable(WorldIglesias.overlayGlasses);
                WorldIglesias.pageOne.drawables.addCamDrawable(WorldIglesias.spriteExplosion);
                imgIglesias++;
            } else if (imgIglesias == 3) {
                WorldIglesias.pageOne.drawables.removeCamDrawable(WorldIglesias.spriteExplosion);
                WorldIglesias.pageOne.drawables.addCamDrawable(WorldIglesias.overlayGorro);
                WorldIglesias.pageOne.drawables.addCamDrawable(WorldIglesias.overlayBolaDer);
                WorldIglesias.pageOne.drawables.addCamDrawable(WorldIglesias.overlayBolaIzq);
                imgIglesias++;
            } else if (imgIglesias == 4){
                WorldIglesias.pageOne.drawables.removeCamDrawable(WorldIglesias.overlayGorro);
                WorldIglesias.pageOne.drawables.removeCamDrawable(WorldIglesias.overlayBolaDer);
                WorldIglesias.pageOne.drawables.removeCamDrawable(WorldIglesias.overlayBolaIzq);
                WorldIglesias.pageOne.drawables.addCamDrawable(WorldIglesias.overlayCowboy);
                imgIglesias = 1;
            } else {
                // Nunca pasara... pero por si acaso!
                alert("no hay mas opciones...");
            }
        }
        // Actualizamos el tiempo del click
        tiempoTouchInicial = tiempoTouch;
    }
};

WorldIglesias.init();