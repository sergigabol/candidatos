var World = {
    // http://www.wikitude.com/external/doc/documentation/latest/Reference/JavaScript%20API/index.html
    init: function(){
        //console.log("entrando init");

        // aqui le agregamos el archivo wtc con
        // las fotos a reconocer obtenido de la web de wikitude con la herramienta TargetManager.
        // Solo puede haber uno habilitado en un mismo momento.

        /*var tracker = new AR.ClientTracker(
            "candidatos.wtc",
            {
            // aqui se pueden pasar opciones
            // Por ejemplo: Cuando se haya cargado correctamente el tracker
            onLoaded: function(){
                        alert("Trackers Cargados !");
                    }
            }

        );*/

        // Creamos el objeto con la  imagen que ha de mostrarse
        var imgRajoy = new AR.ImageResource("rajoy/img_rajoy.png");
        var imgMerkelPirata = new AR.ImageResource("rajoy/merkelpirata.png");


        // Layer donde pintara la imagen
        // Le pasamos
        // imagen, tamanyo relativo, opciones(desplazamiento en la x y etc..)
        var overlayRajoy = new AR.ImageDrawable(
            imgRajoy,
             0.8,
             {
        	    offsetX: 0,
                offsetY: 0
             }
        );

        var overlayMerkelPirata = new AR.ImageDrawable(
            imgMerkelPirata,
             1.2,
             {
        	    offsetX: 0.95,
                offsetY: -0.18
             }
        );
/*
        // Aqui juntamos lo definido anteriormente
        var pageOne = new AR.Trackable2DObject(
            tracker,   // Objeto tracker
            "mariano_rajoy_21",            //Nombre  imagen del tracker a la que se aplica, en este caso a todas
            {
                drawables: {
                    cam: overlayRajoy    // Layer
                }
            }
        );
*/
        var birdImg = new AR.ImageResource("rajoy/bird.png");
        var birdAnimDrawable = new AR.AnimatedImageDrawable(birdImg, 0.3, 64, 64, {
            offsetX: 0.45,
            offsetY: -0.20
        });
        var arr_birdLeft = new Array(13, 14, 15, 16, 15, 14);
        birdAnimDrawable.animate(arr_birdLeft, 100, -1);

        //var geoObject = new AR.GeoObject(geoLocation, {drawables: {cam: birdAnimDrawable,indicator: imageDrawable}});

        var pageOne = new AR.Trackable2DObject(
            tracker,   // Objeto tracker
            "rajoy",            //Nombre  imagen del tracker a la que se aplica, en este caso a todas
            {
                drawables: {
                    cam: [birdAnimDrawable, overlayMerkelPirata]    // Layer
                }
            }
        );

    }

}

World.init();