var WorldRivera={

    init:function(){

        // overlay es un objeto ImageDrawable con una serie de propiedades (se las damos aquí) y ya esta apunto para ser pintado
        var img1=new AR.ImageResource("rivera/donkey_head.png");
        var overlay1=new AR.ImageDrawable(
            img1,
            1.027,
            {
               offsetX:0.1,
               offsetY:0.33
            }
        );
        var img2 = new AR.ImageResource("rivera/bocadillo_pensar.png");
        var overlay2 = new AR.ImageDrawable(
            img2, 0.5,
            {
                offsetX:-0.35,
                offsetY:0.35
            }
        );
        var img3=new AR.ImageResource("rivera/botella.png");
        var botella=new AR.ImageDrawable(
                            img3,
                            0.8,
                            {
                                offsetX:0.25,
                                offsetY:-0.1
                            }
                        );


        var birdImg = new AR.ImageResource("rivera/bird.png");
        var birdAnimDrawable = new AR.AnimatedImageDrawable(birdImg,0.1,64,64,{offsetX:0.05,offsetY:0.6});
        var birdAnimDrawableQuieto = new AR.AnimatedImageDrawable(birdImg,0.3,64,64,{offsetX:0.05,offsetY:0.6});

        var arr_birdLeft = new Array(0, 1, 2, 3, 2, 1,12,13,12,13,12,13,12,13,12,13,12,13,12,13,4,5,6,7);
        var arr_birdquieto = new Array(15,15,15,15,15,15,15,15,15,15,15);
        birdAnimDrawable.animate(arr_birdLeft, 100, -1);

        var birdAnimationScale= new AR.PropertyAnimation(birdAnimDrawable,"scale",1,3,10000,{
                type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_QUAD
            },{
                onFinish:function(){
                   pageOne.drawables.addCamDrawable(birdAnimDrawableQuieto);
                    birdAnimDrawable.enabled=false;
                    birdAnimDrawableQuieto.animate(arr_birdquieto,1000,-1);
                }
            });

          var ampliacion= function(distance){
                    if(distance<500){
                       birdAnimationScale.start();
                    }
                };

        // Aquí juntamos las dos cosas (tracker y overlay[además de las opciones], le pasamos el tracker y un string (para detectar, una, algunas o todas las imagenes del tracker)
        //Array de drawables
        //el asterisco es que la imagen del tracker cumpla esta expresion regular.
        var pageOne=new AR.Trackable2DObject(
            tracker,
            "Alberto_Rivera",
            {
                drawables:{
                    cam:[overlay2, overlay1, botella, birdAnimDrawable] // array amb imatges a superposar al mateix temps
                },
                distanceToTarget:{onDistanceChanged: ampliacion}

            }
        );

    }
};
WorldRivera.init();